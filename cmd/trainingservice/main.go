package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"trainingservice/pkg/config"
	"trainingservice/pkg/server"
	"trainingservice/pkg/service"

	"github.com/caarlos0/env"
	"github.com/sirupsen/logrus"
)

func main() {
	logrus.SetOutput(os.Stdout)

	logrus.Info(server.ServiceName)

	conf := config.Config{}
	flag.Usage = func() {
		flag.CommandLine.SetOutput(os.Stdout)
		for _, val := range conf.HelpDocs() {
			fmt.Println(val)
		}
		fmt.Println("")
		flag.PrintDefaults()
	}
	flag.Parse()

	err := env.Parse(&conf)
	if err != nil {
		logrus.Fatal("invalid environment variable: ", err)
	}

	restfulService := service.New(&conf)

	restfulServer, err := server.New(&conf, restfulService)
	if err != nil {
		logrus.Fatal("unable to initiate server: ", err)
	}

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		logrus.Info("received os quit signal")
		restfulServer.Stop()
	}()

	restfulServer.Serve()
}
