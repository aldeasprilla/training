package api

import (
	"net/http"
	"trainingservice/pkg/service"

	restful "github.com/emicklei/go-restful"
)

// AddRoute adds endpoints to the service
func AddRoute(restfulService *service.Service, basePath string) *restful.WebService {
	webService := new(restful.WebService).Path(basePath).Produces(restful.MIME_JSON)

	webService.Route(webService.
		GET("/hello").To(restfulService.HelloWorld).
		Returns(http.StatusOK, http.StatusText(http.StatusOK), ""))

	return webService
}
