package server

import (
	"fmt"
	"net"
	"net/http"

	"trainingservice/pkg/api"
	"trainingservice/pkg/config"
	"trainingservice/pkg/service"

	restful "github.com/emicklei/go-restful"
	"github.com/sirupsen/logrus"
)

const (
	// ServiceName for start message
	ServiceName = "Training Service"
)

// Server provide state and functionality to run REST API server
type Server struct {
	config    *config.Config
	listener  net.Listener
	container *restful.Container
}

// New creates new Server instances based on configuration
func New(config *config.Config, service *service.Service) (*Server, error) {
	listener, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%s", config.Port))
	if err != nil {
		logrus.Error("unable to create HTTP listener: ", err)
		return nil, err
	}

	server := &Server{
		config:    config,
		listener:  listener,
		container: restful.NewContainer(),
	}

	server.container.Add(api.AddRoute(service, config.BasePath))

	rootWebservice := new(restful.WebService)
	server.container.Add(rootWebservice)

	return server, nil
}

// Stop stops HTTP listener (REST API server)
func (server *Server) Stop() {
	logrus.Debug("stopping server")
	err := server.listener.Close()
	if err != nil {
		logrus.Error("unable to close HTTP listener: ", err)
	}
}

// Serve starts HTTP listener for REST API server
func (server *Server) Serve() {
	logrus.Info("starting server")
	err := http.Serve(server.listener, server.container)
	if err != nil {
		logrus.Error("unable to serve HTTP: ", err)
	}
}
