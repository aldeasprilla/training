package service

import (
	"net/http"

	"trainingservice/pkg/config"

	"github.com/emicklei/go-restful"
	"github.com/sirupsen/logrus"
)

// Service contains service
type Service struct {
	config *config.Config
}

// New creates new service instance with specified DAO object
func New(config *config.Config) *Service {
	return &Service{
		config: config,
	}
}

// GetPlayer handles get players request
func (service *Service) HelloWorld(request *restful.Request, response *restful.Response) {
	err := response.WriteHeaderAndJson(http.StatusOK, "Hello World", restful.DefaultResponseMimeType)
	if err != nil {
		logrus.WithFields(logrus.Fields{"status_code": http.StatusOK, "entity": "Hello World", "mime": restful.DefaultResponseMimeType}).
			Error("unable to write response: ", err)
	}
}
