SVC=trainingservice
BUILDER=accelbyte/golang-builder:1.11.5
export REVISION_ID?=unknown
export BUILD_DATE?=unknown

RUN=docker run --rm \
	-v $(CURDIR):/opt/go/src/$(SVC) \
	-w /opt/go/src/$(SVC)

build:
ifeq ($(OS),Windows_NT)
# Workaround on Windows for https://github.com/golang/dep/issues/1407
	$(RUN) $(BUILDER) rm -rf vendor vendor.orig
	$(RUN) $(BUILDER) rm -rf vendor vendor.orig
endif
	$(RUN) $(BUILDER) dep ensure -v
	$(RUN) -e CGO_ENABLED=0 -e GOOS=linux $(BUILDER) \
		go build -o service -ldflags "-s -X main.revisionID=$(REVISION_ID) -X main.buildDate=$(BUILD_DATE)" \
		./cmd/trainingservice/...
	docker build --tag="$(SVC):$(REVISION_ID)" --tag="$(SVC):latest" .

run:
	docker-compose up