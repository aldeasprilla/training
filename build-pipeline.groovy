#!groovy​

def jobParams = [
    parameters([
        string(name: 'version', defaultValue: '0.0.1', description: 'The version to be build')
    ]),
    [$class: 'BuildDiscarderProperty', strategy: [ $class: 'LogRotator', numToKeepStr: '5']],
    pipelineTriggers([[$class: 'SCMTrigger', scmpoll_spec: 'H/5 * * * *']])
]

properties(jobParams)

def SERVICE="trainingservice"
def gitCommitHash=""
def TAG=""

node('ubuntu-slave-ami') {

    try {

        stage('Checkout') {
            checkout scm
            sh 'git rev-parse --short=8 HEAD > commit'
            gitCommitHash = readFile('commit').trim()
            sh "echo currentVersion=${version}"
            TAG="v${version}.${gitCommitHash}.${env.BUILD_NUMBER}"
            sh "echo tag=${TAG}"
            sh "echo pwd=`pwd`"

            sh 'git rev-parse HEAD > commit'
            gitCommitID = readFile('commit').trim()
            bitbucketStatusNotify(
                buildState: 'INPROGRESS',
                buildKey: 'build',
                buildName: 'Build',
                repoSlug: 'training',
                commitId: "${gitCommitID}"
            )
        }

        stage('Build'){
            sh "make build REVISION_ID=${TAG}"
        }

        stage('Test'){
            sh "make test REVISION_ID=${TAG}"
        }

        if ("${env.BRANCH_NAME}".startsWith("master")){
            deploy()
        }

        bitbucketStatusNotify(
            buildState: 'SUCCESSFUL',
            buildKey: 'build',
            buildName: 'Build',
            repoSlug: 'training',
            commitId: "${gitCommitID}"
        )
    } catch (err) {
        echo "Exception thrown:\n ${err}"
        bitbucketStatusNotify(
            buildState: 'FAILED',
            buildKey: 'build',
            buildName: 'Build',
            buildDescription: "${err}",
            repoSlug: 'training',
            commitId: "${gitCommitID}"
        )
    }
}

def deploy(){
    stage('Upload to ECR') {
        docker.withRegistry("https://${env.ECR_URI}","ecr:${env.AWS_US_WEST_REGION}:ecr-credential") {
            docker.image("${SERVICE}:${TAG}").push("${TAG}")
            docker.image("${SERVICE}:${TAG}").push("latest")
        }
    }

    stage('Cleanup Docker') {
        sh "docker system prune -af"
    }
}