FROM accelbyte/alpine:3.9 as release

COPY service ./service

ENTRYPOINT ["./service"]
