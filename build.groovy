@Library("github.com/AccelByte/jenkins-pipeline-library@dev-lib")_

nodePod(name:"training-build", type:"superbuilder"){
  try {
    stage("Checkout SCM"){
      checkout scm
    }

    container("builder"){
        stage("Build"){
          sh '''
              make build
          '''
        }
    }
  } catch(err) {
    echo "Exception thrown:\n ${err}"
    currentBuild.result = "FAILURE"
  } finally {
    println ("### Image Scan Result: $BUILD_URL/Image_20Scan_20Result ###")
  }
}